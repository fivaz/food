import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import AuthPage from '@/components/auth/AuthPage.vue';
import store from '@/store';

Vue.use(VueRouter);

export const routes: Array<RouteConfig> = [
  {
    path: '*',
    redirect: '/',
  },
  {
    path: '/login',
    name: 'Sign in',
    component: AuthPage,
    meta: {
      public: true,
    },
  },
  {
    path: '/',
    name: 'Meals',
    component: () => import(/* webpackChunkName: "meal" */ '@/components/meal/MealsPage.vue'),
  },
  {
    path: '/ingredients',
    name: 'Ingredients',
    component: () => import(/* webpackChunkName: "ingredient" */ '@/components/ingredient/IngredientsPage.vue'),
  },
  {
    path: '/dishes',
    name: 'Dishes',
    component: () => import(/* webpackChunkName: "dish" */ '@/components/dish/DishesPage.vue'),
  },
  {
    path: '/account',
    name: 'Account',
    component: () => import(/* webpackChunkName: "account" */ '@/components/auth/AccountPage.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((routeTo, routeFrom, next) => {
  if (!routeTo.meta.public && !store.getters.isLoggedIn) {
    return next('login');
  }
  return next();
});

export default router;
