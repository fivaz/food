import Dish from '@/domain/dish/Dish';
import Ingredient from '@/domain/ingredient/Ingredient';
import Meal from '@/domain/meal/Meal';
import User from '@/domain/user/User';

interface State {
  isVisible: boolean,
  dish: Dish,
  ingredient: Ingredient,
  meal: Meal,
  user: User,
}

export default State;
