import Model from '@/domain/Model';

export default class Ingredient extends Model {
  name: string;

  isCountable: boolean;

  price: number;

  quantity: number;

  dishIngredients: { quantity: number };

  constructor(props?: Record<string, unknown>) {
    super();
    this.name = '';
    this.isCountable = false;
    this.price = 0;
    this.quantity = 0;
    this.dishIngredients = { quantity: 0 };
    if (props) {
      Object.assign(this, props);
    }
  }

  get cost(): number {
    if (this.dishIngredients && this.isCountable) {
      return (this.price * this.dishIngredients.quantity) / this.quantity;
    }
    return 0;
  }
}
