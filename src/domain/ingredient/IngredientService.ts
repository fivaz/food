import ModelService from '@/domain/ModelService';
import Ingredient from '@/domain/ingredient/Ingredient';

export default class IngredientService extends ModelService<Ingredient> {
  constructor() {
    super('ingredients', Ingredient);
  }
}
