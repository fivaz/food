interface IModel<T> {
  new(name: Record<string, unknown>): T
}

export default IModel;
