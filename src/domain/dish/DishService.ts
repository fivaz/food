import ModelService from '@/domain/ModelService';
import Dish from '@/domain/dish/Dish';
import IngredientService from '@/domain/ingredient/IngredientService';

export default class DishService extends ModelService<Dish> {
  constructor() {
    super('dishes', Dish);
  }

  buildModel(object: Record<string, unknown>): Dish {
    const dish = new this.Model(object);
    if (object.ingredients && Array.isArray(object.ingredients)) {
      dish.ingredients = object.ingredients
        .map((ingredient) => new IngredientService().buildModel(ingredient));
    }
    return dish;
  }
}
