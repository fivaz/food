import Model from '@/domain/Model';
import Category from '@/helpers/Category';
import Ingredient from '@/domain/ingredient/Ingredient';

export default class Dish extends Model {
  name: string;

  category: Category;

  ingredients: Ingredient[];

  constructor(props?: Record<string, unknown>) {
    super();
    this.name = '';
    this.category = Category.tea;
    this.ingredients = [];
    if (props) {
      Object.assign(this, props);
    }
  }

  get cost(): number {
    const reducer = (total: number, ingredient: Ingredient) => total + ingredient.cost;
    const price = this.ingredients.reduce(reducer, 0);
    return Number(price.toFixed(2));
  }
}
