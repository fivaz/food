import Model from '@/domain/Model';

export default class User extends Model {
  name: string;

  email: string;

  password: string | undefined;

  token: string;

  constructor(props?: Record<string, unknown>) {
    super();
    this.name = '';
    this.email = '';
    this.token = '';
    Object.assign(this, props);
  }
}
