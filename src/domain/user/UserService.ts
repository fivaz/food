import ModelService from '@/domain/ModelService';
import User from '@/domain/user/User';

export default class UserService extends ModelService<User> {
  constructor() {
    super('users', User);
  }
}
