interface AxisError {
  response: {
    data: string
  }
}

export default AxisError;
