import ModelService from '@/domain/ModelService';
import Meal from '@/domain/meal/Meal';
import DishService from '@/domain/dish/DishService';

export default class MealService extends ModelService<Meal> {
  constructor() {
    super('meals', Meal);
  }

  buildModel(object: Record<string, never>): Meal {
    const meal = new this.Model(object);
    meal.date = new Date(object.date);
    meal.dish = new DishService().buildModel(object.dish);
    return meal;
  }
}
