import Meal from '@/domain/meal/Meal';

export default class DateMeal {
  date: string;

  meals: Meal[];

  constructor(firstMeal: Meal) {
    this.date = firstMeal.dateToFr;
    this.meals = [firstMeal];
  }

  addMeal(meal: Meal): void {
    this.meals.push(meal);
  }

  get cost(): number {
    const reducer = (total: number, meal: Meal) => total + meal.dish.cost;
    const price = this.meals.reduce(reducer, 0);
    return Number(price.toFixed(2));
  }
}
