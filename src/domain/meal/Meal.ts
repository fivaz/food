import Model from '@/domain/Model';
import Dish from '@/domain/dish/Dish';
import { DateTime } from 'luxon';

export default class Meal extends Model {
  date: Date;

  dish: Dish;

  dishId: number;

  constructor(props?: Record<string, unknown>) {
    super();
    this.date = new Date();
    this.dish = new Dish();
    this.dishId = 0;
    if (props) {
      Object.assign(this, props);
    }
  }

  get dateToFr(): string {
    return DateTime.fromJSDate(this.date)
      .toLocaleString(DateTime.DATE_SHORT);
  }

  get time(): string {
    return DateTime.fromJSDate(this.date)
      .toLocaleString(DateTime.TIME_SIMPLE);
  }
}
