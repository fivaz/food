import http from '@/http/token-http';
import Model from '@/domain/Model';
import AxisError from '@/domain/AxiosError';
import { AxiosResponse } from 'axios';
import IModel from '@/domain/IModel';

export default abstract class ModelService<T extends Model> {
  protected readonly resource: string;

  protected readonly Model: IModel<T>;

  protected constructor(resource: string, model: IModel<T>) {
    this.resource = resource;
    this.Model = model;
  }

  buildModel(object: Record<string, unknown>): T {
    return new this.Model(object);
  }

  public async index(params = {}): Promise<T[]> {
    return http.get(this.resource, { params })
      .then((res) => res.data)
      .then((models) => models.map((model: Record<string, unknown>) => this.buildModel(model)))
      .catch((err) => ModelService.errorHandler(err));
  }

  public async create(model: Model, params = {}): Promise<T> {
    return http.post(this.resource, model, { params })
      .then((res) => res.data)
      .then((object) => this.buildModel(object))
      .catch((err) => ModelService.errorHandler(err));
  }

  public update(id: number, model: Model, params = {}): Promise<T> {
    return http.put(`${this.resource}/${id}`, model, { params })
      .then((res) => res.data)
      .then((object) => this.buildModel(object))
      .catch((err) => ModelService.errorHandler(err));
  }

  public delete(id: number): Promise<AxiosResponse> {
    return http.delete(`${this.resource}/${id}`)
      .catch((err) => ModelService.errorHandler(err));
  }

  protected static errorHandler(err: AxisError): never {
    console.log(err.response);
    if (err.response.data) {
      throw err.response.data;
    }
    throw err;
  }
}
