import http from '@/http/custom-http';
import store from '@/store';

http.interceptors.request.use((config) => {
  // TODO get state using typescript types
  const { token } = store.state.auth.user;
  if (token) {
    return {
      ...config,
      headers: { Authorization: `Bearer ${token}` },
    };
  }
  return config;
}, (err) => Promise.reject(err));

export default http;
