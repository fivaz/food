import axios from 'axios';

const http = axios.create({
  baseURL: process.env.VUE_APP_API,
  headers: {
    Accept: 'application/json',
    Content: 'application/json',
  },
});

export default http;
