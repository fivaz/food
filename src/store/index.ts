import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

import Modal from '@/store/modules/modal.module';
import Auth from '@/store/modules/auth.module';
import User from '@/domain/user/User';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  modules: ['auth'],
});

interface IAuth {
  user: User
}

interface IState {
  auth: IAuth
}

export default new Vuex.Store<IState>({
  modules: {
    modal: Modal,
    auth: Auth,
  },
  plugins: [vuexLocal.plugin],
});
