import {
  Action, Module, Mutation, VuexModule,
} from 'vuex-module-decorators';
import User from '@/domain/user/User';

@Module({
  name: 'auth',
  preserveState: true,
})
class Auth extends VuexModule {
  user = new User();

  @Mutation
  setUser(newUser: User): void {
    this.user = newUser;
  }

  @Mutation
  resetUser(): void {
    this.user = new User();
  }

  @Action
  login(user: User): void {
    this.context.commit('setUser', user);
  }

  @Action
  logout(): void {
    this.context.commit('resetUser');
  }

  get isLoggedIn(): boolean {
    return !!this.user?.token;
  }
}

export default Auth;
