import {
  Action, Module, Mutation, VuexModule,
} from 'vuex-module-decorators';
import Dish from '@/domain/dish/Dish';
import Ingredient from '@/domain/ingredient/Ingredient';
import Meal from '@/domain/meal/Meal';

@Module({
  name: 'modal',
  namespaced: true,
})
export default class Modal extends VuexModule {
  isVisible = false;

  dish = new Dish();

  ingredient = new Ingredient();

  meal = new Meal();

  @Mutation
  showModal(): void {
    this.isVisible = true;
  }

  @Mutation
  hideModal(): void {
    this.isVisible = false;
  }

  @Mutation
  setIngredient(newIngredient: Ingredient): void {
    this.ingredient = newIngredient;
  }

  @Mutation
  setDish(newDish: Dish): void {
    this.dish = newDish;
  }

  @Mutation
  setMeal(newMeal: Meal): void {
    this.meal = newMeal;
  }

  @Action
  openModal(object?: unknown): void {
    // TODO refactor this
    if (object instanceof Ingredient) {
      this.context.commit('setIngredient', object);
    } else if (object instanceof Dish) {
      this.context.commit('setDish', object);
    } else if (object instanceof Meal) {
      this.context.commit('setMeal', object);
    }
    this.context.commit('showModal');
  }

  @Action
  closeModal(): void {
    this.context.commit('hideModal');
  }
}
