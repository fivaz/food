enum Unit {
  g = 'g',
  l = 'l',
  kg = 'kg',
  ml = 'ml',
  unit = 'unit',
}

export default Unit;
