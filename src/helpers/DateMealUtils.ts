import DateMeal from '@/domain/meal/date-meal/DateMeal';
import Meal from '@/domain/meal/Meal';

export default class DateMealUtils {
  static convert(meals: Meal[]): IterableIterator<DateMeal> {
    const map = new Map<string, DateMeal>();

    meals.forEach((meal) => {
      const dateMeal = map.get(meal.dateToFr);
      if (dateMeal) {
        dateMeal.addMeal(meal);
      } else {
        map.set(meal.dateToFr, new DateMeal(meal));
      }
    });
    return map.values();
  }
}
