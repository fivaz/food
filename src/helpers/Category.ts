enum Category {
  breakfast = 'breakfast',
  brunch = 'brunch',
  lunch = 'lunch',
  tea = 'tea',
  supper = 'supper',
  dinner = 'dinner',
}

export default Category;
